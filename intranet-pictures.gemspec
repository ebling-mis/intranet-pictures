# frozen_string_literal: true

require 'date'
require_relative 'lib/intranet/pictures/version'

Gem::Specification.new do |s|
  s.name        = Intranet::Pictures::NAME
  s.version     = Intranet::Pictures::VERSION

  s.summary     = 'Pictures gallery module for the intranet.'
  s.homepage    = Intranet::Pictures::HOMEPAGE_URL
  s.metadata    = { 'source_code_uri' => Intranet::Pictures::SOURCES_URL }
  s.license     = 'MIT'

  s.author      = 'Ebling Mis'
  s.email       = 'ebling.mis@protonmail.com'

  # Make sure the gem is built from versioned files
  s.files         = `git ls-files -z`.split("\0").grep(/^spec|^lib|^README/)
  s.require_paths = %w[lib]

  s.required_ruby_version = '~> 3.0'

  s.add_dependency 'intranet-core', '~> 2.0', '>= 2.4.1'
  s.add_dependency 'mimemagic', '~> 0.4'
end
