# frozen_string_literal: true

require 'bundler/setup'
require 'rspec/core/rake_task'
require 'rubocop/rake_task'
require 'yard'

$LOAD_PATH << File.absolute_path('lib', __dir__)

task default: %w[rspec perfo rubocop doc]

desc 'Run all RSpec tests'
RSpec::Core::RakeTask.new(:rspec)

desc 'Run performance tests'
task :perfo do
  require_relative 'perfo/runner'
  Runner.run
end

desc 'Run RuboCop to detect coding rules violations'
RuboCop::RakeTask.new(:rubocop)

desc 'Generate documentation (using Yard)'
YARD::Rake::YardocTask.new(:doc) do |task|
  task.stats_options = ['--list-undoc']
end
