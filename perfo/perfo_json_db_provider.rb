# frozen_string_literal: true

require 'fileutils'
require 'json'
require 'core_extensions'
require 'intranet/pictures/json_db_provider'

# Manager of a performance test suite.
class PerfoJsonDbProvider
  def initialize(num_pictures = 5000, num_group_types = 10, num_groups = 50)
    @num_pictures    = num_pictures
    @num_group_types = num_group_types
    @num_groups      = num_groups
    @db_file         = File.join(__dir__, 'bench-db.json')
  end

  def setup
    create_large_json_db(@db_file)
    @provider = Intranet::Pictures::JsonDbProvider.new(@db_file)
  end

  def exercise(suite)
    sel = { 'group1' => 'group1_title1' }
    suite.add_suite('JsonDbProvider') do |x|
      x.report('#list_pictures (all)')        { @provider.list_pictures }
      x.report('#list_pictures (sel)')        { @provider.list_pictures(sel) }
      x.report('#list_pictures (all+sort)')   { @provider.list_pictures({}, 'title', true) }
      x.report('#list_pictures (sel+sort)')   { @provider.list_pictures(sel, 'title', true) }
      x.report('#list_pictures (all+rsort)')  { @provider.list_pictures({}, 'title', false) }
      x.report('#list_pictures (sel+rsort)')  { @provider.list_pictures(sel, 'title', false) }
    end
  end

  def teardown
    FileUtils.rm_f(@db_file)
  end

  private

  def random_string(len)
    len.times.map { rand(48..126).chr }.join
  end

  def db_add_groups(db)
    @num_group_types.times do |type_num|
      db[:groups]["group#{type_num}"] = @num_groups.times.map do |group_num|
        {
          id: "group#{type_num}_title#{group_num}",
          title: "Group #{type_num}, title #{group_num}",
          brief: random_string(16)
        }
      end
    end
  end

  def db_add_picture(db, img_num)
    img = { id: img_num, uri: "./#{img_num}.jpg", title: random_string(16) }
    @num_group_types.times do |type_num|
      group_num = rand(@num_groups * 1.2) # 20% chance of not being in this group type
      img["group#{type_num}"] = "group#{type_num}_title#{group_num}" unless group_num >= @num_groups
    end
    db[:pictures] << img
  end

  def create_large_json_db(file)
    db = {
      groups: {},
      pictures: []
    }
    db_add_groups(db)
    @num_pictures.times { |img_num| db_add_picture(db, img_num) }
    File.write(file, db.to_json)
  end
end
