# frozen_string_literal: true

require 'core_extensions'
require_relative 'suite'

# Runner for a performance test suite
module Runner
  class << self
    def run(num_iterations = 10, label_length = 32)
      suite = Suite.new(num_iterations, label_length)
      run_all_modules(suite)
    end

    private

    def run_all_modules(suite)
      available_modules.each do |name|
        obj = instanciate_module(name)
        obj.setup
        obj.exercise(suite)
      ensure
        obj&.teardown
      end
    end

    def available_modules
      Dir.glob(File.join(__dir__, 'perfo_*.rb')).map do |path|
        File.basename(path, File.extname(path))
      end
    end

    def instanciate_module(name)
      require_relative name
      Object.const_get(name.humanize.tr(' ', '')).new
    end
  end
end
