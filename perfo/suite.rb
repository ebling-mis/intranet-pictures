# frozen_string_literal: true

require 'benchmark'

# Manager of a performance test suite.
# Test suite is divided into several (sub)suites, each forming to a consistent set of performance
# measurements (typically measurements from different services of a same module or class).
# @example Create a (sub)suite and add time measurements to it
#  s = Suite.new
#  i = MyClassName.new
#  s.add_suite('MyClassName') do |x|
#    x.report('#InstanceMethod1') { i.InstanceMethod1 }
#    x.report('#InstanceMethod2') { i.InstanceMethod2 }
#    # and so on...
#  end
class Suite
  def initialize(num_iterations, label_length)
    puts "Running benchmarks (#{num_iterations} iterations)..."
    @num_iterations = num_iterations
    @label_length   = label_length
  end

  def add_suite(name)
    report = Report.new(name, @num_iterations, @label_length)
    yield(report)
    report.finalize
  end

  # Internal class used when yield-ing a subsuite. Responsible for aggregating time measurements
  # inside a subsuite created by Suite#add_suite.
  class Report
    attr_reader :results

    def initialize(name, num_iterations, label_length)
      @num_iterations = num_iterations
      @label_length   = label_length
      puts "#{name} #{'-' * [@label_length - name.length + 15, 0].max}"
    end

    def finalize
      puts ''
    end

    def report(metric, &block)
      time = Benchmark.realtime do
        @num_iterations.times(&block)
      end
      puts "  #{metric} #{' ' * [@label_length - metric.length, 0].max}  #{format('%10.6f', time)}s"
    end
  end
end
