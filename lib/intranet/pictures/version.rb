# frozen_string_literal: true

# The main Intranet namespace.
module Intranet
  # The Pictures gallery module for the Intranet.
  module Pictures
    # The name of the gem.
    NAME = 'intranet-pictures'

    # The version of the gem, according to semantic versionning.
    VERSION = '3.0.0.rc1'

    # The URL of the gem homepage.
    HOMEPAGE_URL = 'https://rubygems.org/gems/intranet-pictures'

    # The URL of the gem source code.
    SOURCES_URL = 'https://bitbucket.org/ebling-mis/intranet-pictures'
  end
end
