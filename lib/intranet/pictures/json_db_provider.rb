# frozen_string_literal: true

require 'json'
require 'mimemagic'

module Intranet
  module Pictures
    # Provides pictures data and pictures listings from a database file in JSON format.
    #
    # === Structure of the JSON database
    # See the example below.
    #
    # Pictures are described individually as a hash in the +pictures+ array. The mandatory keys in
    # this hash are:
    # * +uri+ : location of the picture, relative to the JSON file (string)
    # * +title+ : description of the picture (string)
    # * +datetime+ : date and time of the picture, in ISO8601 format (string)
    # * +height+ and +width+ : size in pixels of the picture (integers)
    #
    # Additional keys may be added (for instance: event, city, region/country, ...); they will be
    # used to select pictures sharing the same value of a given key, or to group them.
    # Values may either be singular string elements (for instance: "My wonderful event") or array
    # of strings (for instance: [ "Casino", "Las Vegas", "Urban photography" ]).
    #
    # Some groups may be defined in the +groups+ hash to add a thumbnail picture and a brief text
    # description for them. The possible keys in this hash are:
    # * +id+ : unique identifier of the group, mandatory (string)
    # * +brief+ : optional short text associated to the group name (string)
    # * +uri+ : optional group thumbnail, relative to the JSON file (string)
    #
    # @example Structure of the JSON database (not all mandatory keys are present for readability)
    #   {
    #     "groups": {
    #       "event": [
    #         { "id": "Party", "brief": "...", "uri": "party.jpg", ... },
    #         { ... }
    #       ],
    #       "city": [
    #         { "id": "Houston", "uri": "houston.png", ... },
    #         { ... }
    #       ]
    #     },
    #     "pictures": [
    #       { "uri": "dir/pic0.jpg", "datetime": "...", "event": "Party", "city": "Houston",
    #         "tags": [ "USA", "Portrait" ], ... },
    #       { ... }
    #     ]
    #   }
    class JsonDbProvider
      # Initializes a new pictures data provider.
      # @param json_file [String] The path to the JSON database file.
      def initialize(json_file)
        @json_dir = File.dirname(json_file) # also works for URLs
        @json_file = json_file
        @json_time = Time.at(0)
        load_json
      end

      # Returns the list of the pictures matching the given selectors.
      # Selectors are key/value strings pairs restricting the pictures to be returned:
      #   - if a picture has the given key, its value is a singular string and is strictly equal
      #     to the given value, it is returned;
      #   - if a picture has the given key, its value is an array of strings and ONE of the
      #     elements of this array is equal to the given value, it is returned;
      #   - all other pictures are not returned.
      # Results are sorted according to the value of the key +sort_by+, in ascending order if
      # +asc+, and in descending order otherwise. Sorting by a key whose value is not a singular
      # string is not defined.
      # @param selector [Hash<String,String>] The pictures selector, interpreted as a logical AND
      #                                       combination of all key/value pairs provided.
      # @param sort_by [String] The picture field to sort the results by, or nil if results should
      #                         be returned without particular sorting.
      # @param asc [Boolean] True to sort returned pictures in ascending order, False to sort them
      #                      in descending order.
      # @return [Array<Hash{'title'=>String, 'datetime'=>String, 'height'=>Integer,
      #  'width'=>Integer, ...}>] The selected pictures.
      # @raise KeyError If JSON file is malformed, or if +sort_by+ is not an existing picture field,
      #                 or if +sort_by+ is not a singular string value.
      def list_pictures(selector = {}, sort_by = nil, asc = true)
        load_json
        pics = select_pictures(selector).map { |p| p.except('uri') }
        unless sort_by.nil?
          raise KeyError if pics.any? { |p| p.fetch(sort_by).respond_to?('any?') }

          pics.sort_by! { |p| p.fetch(sort_by) }
        end
        pics.reverse! unless asc
        pics
      end

      # Returns the picture file matching +selector+.
      # @param selector [Hash<String,String>] The picture selector, which should return exactly one
      #                                       picture.
      # @return [String, Blob] The MIME type of the picture, and the picture file content.
      # @raise KeyError If JSON file is malformed, if selector does not match exactly one picture,
      #                 or if the picture +uri+ does not refer to an existing image file.
      def picture(selector = {})
        load_json
        pic = select_pictures(selector)
        raise KeyError unless pic.size == 1

        path = File.join(@json_dir, pic.first.fetch('uri'))
        read_image_file(path)
      end

      # Returns the thumbnail picture of the group whose type is +key+ and named +value+.
      # @param key [String] The group type.
      # @param value [String] The group name.
      # @return [String, Blob] The MIME type of the picture, and the picture file content. Nil may
      #                        be returned if no thumbnail is available for that group.
      # @raise KeyError If JSON file is malformed, if key/value do not designate exactly one group,
      #                 or if the group +uri+ does not refer to an existing image file.
      def group_thumbnail(key, value)
        load_json
        group = select_group(key, value)
        raise KeyError unless group.size == 1
        return nil if group.first['uri'].nil?

        path = File.join(@json_dir, group.first.fetch('uri'))
        read_image_file(path)
      end

      # Returns the brief text of the group whose type is +key+ and named +value+.
      # @param key [String] The group type.
      # @param value [String] The group name.
      # @return [String] The brief text of the group, or an empty string if no brief is available.
      # @raise KeyError If JSON file is malformed or if key/value do not designate exactly one
      #                 group.
      def group_brief(key, value)
        load_json
        group = select_group(key, value)
        raise KeyError unless group.size == 1
        return '' if group.first['brief'].nil?

        group.first.fetch('brief')
      end

      private

      # (Re)load the JSON file if modified on disk
      def load_json
        mtime = File.mtime(@json_file)
        return unless @json_time < File.mtime(@json_file)

        @json = JSON.parse(File.read(@json_file))
        @json_time = mtime
      end

      def read_image_file(path)
        mime_type = MimeMagic.by_path(path).type
        raise KeyError unless mime_type.start_with?('image/')

        [MimeMagic.by_path(path).type, File.read(path)]
      rescue Errno::ENOENT
        raise KeyError
      end

      def picture_match?(picture, selector)
        selector.all? do |k, v|
          picture_value = picture.fetch(k)
          if picture_value.respond_to?('any?')
            picture_value.any? { |e| e == v }
          else
            picture_value == v
          end
        end
      rescue KeyError
        false
      end

      def select_pictures(selector)
        @json.fetch('pictures').select { |p| picture_match?(p, selector) }
      end

      def select_group(type, id)
        @json.fetch('groups').fetch(type).select { |g| g.fetch('id') == id }
      end
    end
  end
end
