# frozen_string_literal: true

require 'intranet/core'
require 'intranet/logger'
require 'intranet/abstract_responder'
require 'intranet/pictures/responder'

RSpec.describe Intranet::Pictures::Responder do
  it 'should inherit from Intranet::AbstractResponder' do
    expect(described_class.superclass).to eql(Intranet::AbstractResponder)
  end

  it 'should define its name, version and homepage' do
    expect { described_class.module_name }.not_to raise_error
    expect { described_class.module_version }.not_to raise_error
    expect { described_class.module_homepage }.not_to raise_error
  end

  before do
    logger = Intranet::Logger.new(Intranet::Logger::FATAL)
    @core = Intranet::Core.new(logger)

    @provider = Intranet::Pictures::JsonDbProvider.new(File.join(__dir__, 'sample-db.json'))
    defaults = { 'group_by' => 'location', 'sort_by' => 'datetime', 'sort_order' => 'desc' }
    @responder = described_class.new(@provider, defaults)
    @core.register_module(
      @responder, ['pictures'], File.absolute_path('../../../lib/intranet/resources', __dir__)
    )
  end

  describe '#in_menu?' do
    it 'should return the value provided at initialization' do
      expect(described_class.new(nil, {}, false).in_menu?).to be false
      expect(described_class.new(nil, {}, true).in_menu?).to be true
    end
  end

  describe '#resources_dir' do
    it 'should return the absolute path of the resources directory' do
      expect(described_class.new(nil, {}, false).resources_dir).to eql(
        File.absolute_path('../../../lib/intranet/resources', __dir__)
      )
    end
  end

  describe '#title' do
    it 'should return the title of the webpage provided by the module' do
      expect(@responder.title).to eql(I18n.t('pictures.menu'))
    end
  end

  describe '#generate_page' do
    context 'when asked for \'/index.html\'' do
      it 'should return a partial HTML content with selected pictures grouped as requested' do
        # No parameter in query: use default configuration provided at initialization & show all pictures
        query = {}
        code, mime, content = @responder.generate_page('/index.html', query)
        expect(code).to eql(206)
        expect(mime).to eql('text/html')
        expect(content).to eql(
          {
            content: "<section>\n<h2>#{I18n.t('pictures.menu')}</h2>\n" \
                     "<ul class='breadcrumb'>\n" \
                     "<li>\n<a href='/index.html'>#{I18n.t('nav.home')}</a>\n</li>\n" \
                     "<li>#{I18n.t('pictures.menu')}</li>\n" \
                     "</ul>\n\n" \
                     "<h4>Tags</h4>\n" \
                     "<div id='tags'>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Cherry blossom&quot;);' type='button'>\nCherry blossom\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Louvre&quot;);' type='button'>\nLouvre\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;MoMAK&quot;);' type='button'>\nMoMAK\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Modern art&quot;);' type='button'>\nModern art\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Pyramid&quot;);' type='button'>\nPyramid\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Urban photography&quot;);' type='button'>\nUrban photography\n</button>\n" \
                     "</div>\n" \
                     "<h4>#{I18n.t('pictures.nav.location')}</h4>\n" \
                     "<ul class='groups'>\n" \
                     "<li title='New York, USA'>\n<a onclick='openImagesGallery(&quot;location=New York, USA&amp;sort_by=datetime&amp;sort_order=asc&quot;);'>\n<figure>\n<div style='background-image: url(&quot;api/group_thumbnail?location=New York, USA&quot;)'></div>\n<figcaption>\nNew York, USA\n</figcaption>\n</figure>\n</a>\n</li>\n" \
                     "<li title='Paris, France'>\n<a onclick='openImagesGallery(&quot;location=Paris, France&amp;sort_by=datetime&amp;sort_order=asc&quot;);'>\n<figure>\n<div style='background-image: url(&quot;api/group_thumbnail?location=Paris, France&quot;)'></div>\n<figcaption>\nParis, France\n<br>\n<em>The City of Light</em>\n</figcaption>\n</figure>\n</a>\n</li>\n" \
                     "<li title='Tokyo, Japan'>\n<a onclick='openImagesGallery(&quot;location=Tokyo, Japan&amp;sort_by=datetime&amp;sort_order=asc&quot;);'>\n<figure>\n<div style='background-image: url(&quot;api/group_thumbnail?location=Tokyo, Japan&quot;)'></div>\n<figcaption>\nTokyo, Japan\n</figcaption>\n</figure>\n</a>\n</li>\n" \
                     "</ul>\n" \
                     "</section>\n",
            title: I18n.t('pictures.menu'),
            stylesheets: [
              'design/style.css',
              'design/photoswipe/photoswipe.css',
              'design/photoswipe/photoswipe-dynamic-caption-plugin.css'
            ],
            scripts: [{ src: 'design/jpictures.js', type: 'module' }]
          }
        )

        # Query overrides group_by field
        query = { 'group_by' => 'author' }
        code, mime, content = @responder.generate_page('/index.html', query)
        expect(code).to eql(206)
        expect(mime).to eql('text/html')
        expect(content).to eql(
          {
            content: "<section>\n<h2>#{I18n.t('pictures.menu')}</h2>\n" \
                     "<ul class='breadcrumb'>\n" \
                     "<li>\n<a href='/index.html'>#{I18n.t('nav.home')}</a>\n</li>\n" \
                     "<li>#{I18n.t('pictures.menu')}</li>\n" \
                     "</ul>\n\n" \
                     "<h4>Tags</h4>\n" \
                     "<div id='tags'>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Cherry blossom&quot;);' type='button'>\nCherry blossom\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Louvre&quot;);' type='button'>\nLouvre\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;MoMAK&quot;);' type='button'>\nMoMAK\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Modern art&quot;);' type='button'>\nModern art\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Pyramid&quot;);' type='button'>\nPyramid\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Urban photography&quot;);' type='button'>\nUrban photography\n</button>\n" \
                     "</div>\n" \
                     "<h4>#{I18n.t('pictures.nav.author')}</h4>\n" \
                     "<ul class='groups'>\n" \
                     "<li title='Jane Doe'>\n<a onclick='openImagesGallery(&quot;author=Jane Doe&amp;sort_by=datetime&amp;sort_order=asc&quot;);'>\n<figure>\n<div style='background-image: url(&quot;api/group_thumbnail?author=Jane Doe&quot;)'></div>\n<figcaption>\nJane Doe\n</figcaption>\n</figure>\n</a>\n</li>\n" \
                     "<li title='John Doe'>\n<a onclick='openImagesGallery(&quot;author=John Doe&amp;sort_by=datetime&amp;sort_order=asc&quot;);'>\n<figure>\n<div style='background-image: url(&quot;api/group_thumbnail?author=John Doe&quot;)'></div>\n<figcaption>\nJohn Doe\n<br>\n<em>Best photographer ever</em>\n</figcaption>\n</figure>\n</a>\n</li>\n" \
                     "</ul>\n" \
                     "</section>\n",
            title: I18n.t('pictures.menu'),
            stylesheets: [
              'design/style.css',
              'design/photoswipe/photoswipe.css',
              'design/photoswipe/photoswipe-dynamic-caption-plugin.css'
            ],
            scripts: [{ src: 'design/jpictures.js', type: 'module' }]
          }
        )

        # Query overrides group_by field. This field is present for some pictures only.
        query = { 'group_by' => 'camera' }
        code, mime, content = @responder.generate_page('/index.html', query)
        expect(code).to eql(206)
        expect(mime).to eql('text/html')
        expect(content).to eql(
          {
            content: "<section>\n<h2>#{I18n.t('pictures.menu')}</h2>\n" \
                     "<ul class='breadcrumb'>\n" \
                     "<li>\n<a href='/index.html'>#{I18n.t('nav.home')}</a>\n</li>\n" \
                     "<li>#{I18n.t('pictures.menu')}</li>\n" \
                     "</ul>\n\n" \
                     "<h4>Tags</h4>\n" \
                     "<div id='tags'>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Cherry blossom&quot;);' type='button'>\nCherry blossom\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Louvre&quot;);' type='button'>\nLouvre\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;MoMAK&quot;);' type='button'>\nMoMAK\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Modern art&quot;);' type='button'>\nModern art\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Pyramid&quot;);' type='button'>\nPyramid\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Urban photography&quot;);' type='button'>\nUrban photography\n</button>\n" \
                     "</div>\n" \
                     "<h4>#{I18n.t('pictures.nav.camera')}</h4>\n" \
                     "<ul class='groups'>\n" \
                     "<li title='Canon EOS 5D MARK IV'>\n<a onclick='openImagesGallery(&quot;camera=Canon EOS 5D MARK IV&amp;sort_by=datetime&amp;sort_order=asc&quot;);'>\n<figure>\n<div style='background-image: url(&quot;api/group_thumbnail?camera=Canon EOS 5D MARK IV&quot;)'></div>\n<figcaption>\nCanon EOS 5D MARK IV\n</figcaption>\n</figure>\n</a>\n</li>\n" \
                     "<li title='Apple iPhone 11'>\n<a onclick='openImagesGallery(&quot;camera=Apple iPhone 11&amp;sort_by=datetime&amp;sort_order=asc&quot;);'>\n<figure>\n<div style='background-image: url(&quot;api/group_thumbnail?camera=Apple iPhone 11&quot;)'></div>\n<figcaption>\nApple iPhone 11\n<br>\n<em>Best smartphone ever</em>\n</figcaption>\n</figure>\n</a>\n</li>\n" \
                     "</ul>\n" \
                     "</section>\n",
            title: I18n.t('pictures.menu'),
            stylesheets: [
              'design/style.css',
              'design/photoswipe/photoswipe.css',
              'design/photoswipe/photoswipe-dynamic-caption-plugin.css'
            ],
            scripts: [{ src: 'design/jpictures.js', type: 'module' }]
          }
        )

        # Query overrides defaults & specifies a valid selector
        query = { 'group_by' => 'location', 'author' => 'John Doe', 'sort_by' => 'location', 'sort_order' => 'desc' }
        code, mime, content = @responder.generate_page('/index.html', query)
        expect(code).to eql(206)
        expect(mime).to eql('text/html')
        expect(content).to eql(
          {
            content: "<section>\n<h2>#{I18n.t('pictures.menu')}</h2>\n" \
                     "<ul class='breadcrumb'>\n" \
                     "<li>\n<a href='/index.html'>#{I18n.t('nav.home')}</a>\n</li>\n" \
                     "<li>#{I18n.t('pictures.menu')}</li>\n" \
                     "</ul>\n\n" \
                     "<h4>Tags</h4>\n" \
                     "<div id='tags'>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Louvre&quot;);' type='button'>\nLouvre\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Modern art&quot;);' type='button'>\nModern art\n</button>\n" \
                     "</div>\n" \
                     "<h4>#{I18n.t('pictures.nav.location')}</h4>\n" \
                     "<ul class='groups'>\n" \
                     "<li title='Tokyo, Japan'>\n<a onclick='openImagesGallery(&quot;author=John Doe&amp;location=Tokyo, Japan&amp;sort_by=datetime&amp;sort_order=asc&quot;);'>\n<figure>\n<div style='background-image: url(&quot;api/group_thumbnail?location=Tokyo, Japan&quot;)'></div>\n<figcaption>\nTokyo, Japan\n</figcaption>\n</figure>\n</a>\n</li>\n" \
                     "<li title='Paris, France'>\n<a onclick='openImagesGallery(&quot;author=John Doe&amp;location=Paris, France&amp;sort_by=datetime&amp;sort_order=asc&quot;);'>\n<figure>\n<div style='background-image: url(&quot;api/group_thumbnail?location=Paris, France&quot;)'></div>\n<figcaption>\nParis, France\n<br>\n<em>The City of Light</em>\n</figcaption>\n</figure>\n</a>\n</li>\n" \
                     "</ul>\n" \
                     "</section>\n",
            title: I18n.t('pictures.menu'),
            stylesheets: [
              'design/style.css',
              'design/photoswipe/photoswipe.css',
              'design/photoswipe/photoswipe-dynamic-caption-plugin.css'
            ],
            scripts: [{ src: 'design/jpictures.js', type: 'module' }]
          }
        )

        # Invalid selector
        query = { 'group_by' => 'location', 'foo' => 'bar' }
        code, mime, content = @responder.generate_page('/index.html', query)
        expect(code).to eql(206)
        expect(mime).to eql('text/html')
        expect(content).to eql(
          {
            content: "<section>\n<h2>#{I18n.t('pictures.menu')}</h2>\n" \
                     "<ul class='breadcrumb'>\n" \
                     "<li>\n<a href='/index.html'>#{I18n.t('nav.home')}</a>\n</li>\n" \
                     "<li>#{I18n.t('pictures.menu')}</li>\n" \
                     "</ul>\n\n" \
                     "<h4>Tags</h4>\n" \
                     "<p>\n<em>#{I18n.t('pictures.no_result_found')}</em>\n</p>\n" \
                     "<h4>#{I18n.t('pictures.nav.location')}</h4>\n" \
                     "<p>\n<em>#{I18n.t('pictures.no_result_found')}</em>\n</p>\n" \
                     "</section>\n",
            title: I18n.t('pictures.menu'),
            stylesheets: [
              'design/style.css',
              'design/photoswipe/photoswipe.css',
              'design/photoswipe/photoswipe-dynamic-caption-plugin.css'
            ],
            scripts: [{ src: 'design/jpictures.js', type: 'module' }]
          }
        )

        # Invalid grouping criteria
        query = { 'group_by' => 'foo' }
        code, mime, content = @responder.generate_page('/index.html', query)
        expect(code).to eql(206)
        expect(mime).to eql('text/html')
        expect(content).to eql(
          {
            content: "<section>\n<h2>#{I18n.t('pictures.menu')}</h2>\n" \
                     "<ul class='breadcrumb'>\n" \
                     "<li>\n<a href='/index.html'>#{I18n.t('nav.home')}</a>\n</li>\n" \
                     "<li>#{I18n.t('pictures.menu')}</li>\n" \
                     "</ul>\n\n" \
                     "<h4>Tags</h4>\n" \
                     "<div id='tags'>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Cherry blossom&quot;);' type='button'>\nCherry blossom\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Louvre&quot;);' type='button'>\nLouvre\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;MoMAK&quot;);' type='button'>\nMoMAK\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Modern art&quot;);' type='button'>\nModern art\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Pyramid&quot;);' type='button'>\nPyramid\n</button>\n" \
                     "<button class='tag' onclick='filterByTag(&quot;Urban photography&quot;);' type='button'>\nUrban photography\n</button>\n" \
                     "</div>\n" \
                     "<h4>#{I18n.t('pictures.nav.foo')}</h4>\n" \
                     "<p>\n<em>#{I18n.t('pictures.no_result_found')}</em>\n</p>\n" \
                     "</section>\n",
            title: I18n.t('pictures.menu'),
            stylesheets: [
              'design/style.css',
              'design/photoswipe/photoswipe.css',
              'design/photoswipe/photoswipe-dynamic-caption-plugin.css'
            ],
            scripts: [{ src: 'design/jpictures.js', type: 'module' }]
          }
        )

        # Invalid sorting criteria
        query = { 'group_by' => 'location', 'sort_order' => 'foo' }
        code, mime, content = @responder.generate_page('/index.html', query)
        expect(code).to eql(404)
        expect(mime).to be_empty
        expect(content).to be_empty
      end
    end

    context 'when asked for \'/i18n.js\'' do
      it 'should return the internationalized version of required JavaScript variables' do
        code, mime, content = @responder.generate_page('/i18n.js', {})
        expect(code).to eql(200)
        expect(mime).to eql('text/javascript')
        expect(content).to eql(
          "export default {\n" \
          "  viewer_close: '#{I18n.t('pictures.viewer.close')}',\n" \
          "  viewer_zoom: '#{I18n.t('pictures.viewer.zoom')}',\n" \
          "  viewer_previous: '#{I18n.t('pictures.viewer.previous')}',\n" \
          "  viewer_next: '#{I18n.t('pictures.viewer.next')}',\n" \
          "  viewer_toggle_caption: '#{I18n.t('pictures.viewer.toggle_caption')}' };"
        )
      end
    end

    context 'when asked for \'/api/group_thumbnail\'' do
      it 'should return the selected group thumnail' do
        # Existing group with thumbnail
        query = { 'location' => 'New York, USA' }
        code, mime, content = @responder.generate_page('/api/group_thumbnail', query)
        expect(code).to eql(200)
        expect(mime).to eql('image/png')
        expect(content).to eql(File.read(File.join(__dir__, 'alpha.png')))

        # Existing group with no specified thumbnail
        query = { 'location' => 'Paris, France' }
        code, mime, content = @responder.generate_page('/api/group_thumbnail', query)
        expect(code).to eql(200)
        expect(mime).to eql('image/svg+xml')
        expect(content).to eql(
          File.read(File.join(__dir__, '../../../lib/intranet/resources/www/group_thumbnail.svg'))
        )

        # Existing group with non-existant thumbnail
        query = { 'author' => 'Jane Doe' }
        code, mime, content = @responder.generate_page('/api/group_thumbnail', query)
        expect(code).to eql(404)
        expect(mime).to be_empty
        expect(content).to be_empty

        # Invalid query
        query = { 'location' => 'Tokyo, Japan', 'camera' => 'Canon EOS 5D MARK IV' }
        code, mime, content = @responder.generate_page('/api/group_thumbnail', query)
        expect(code).to eql(404)
        expect(mime).to be_empty
        expect(content).to be_empty
      end
    end

    context 'when asked for \'/api/group_brief\'' do
      it 'should return the selected group brief text' do
        # Existing group with brief text
        query = { 'location' => 'Paris, France' }
        code, mime, content = @responder.generate_page('/api/group_brief', query)
        expect(code).to eql(200)
        expect(mime).to eql('application/json')
        expect(content).to eql('The City of Light'.to_json)

        # Existing group with no brief text
        query = { 'location' => 'Tokyo, Japan' }
        code, mime, content = @responder.generate_page('/api/group_brief', query)
        expect(code).to eql(200)
        expect(mime).to eql('application/json')
        expect(content).to eql(''.to_json)

        # Non-existing group
        query = { 'flash' => 'true' }
        code, mime, content = @responder.generate_page('/api/group_brief', query)
        expect(code).to eql(404)
        expect(mime).to be_empty
        expect(content).to be_empty

        # Invalid query
        query = { 'location' => 'Tokyo, Japan', 'camera' => 'Canon EOS 5D MARK IV' }
        code, mime, content = @responder.generate_page('/api/group_brief', query)
        expect(code).to eql(404)
        expect(mime).to be_empty
        expect(content).to be_empty
      end
    end

    context 'when asked for \'/api/pictures\'' do
      it 'should return a JSON representation of the selected pictures' do
        # All pictures (no selector & default sort order)
        code, mime, content = @responder.generate_page('/api/pictures', {})
        expect(code).to eql(200)
        expect(mime).to eql('application/json')
        expect(content).to eql(
          [
            { 'datetime' => '2020:06:20 18:14:09', 'flash' => 'true', 'author' => 'Jane Doe', 'location' => 'New York, USA', 'tags' => 'Urban photography' },
            { 'datetime' => '2020:06:20 06:09:54', 'flash' => 'true', 'author' => 'Jane Doe', 'location' => 'Paris, France', 'camera' => 'Canon EOS 5D MARK IV', 'tags' => ['Louvre', 'Pyramid', 'Urban photography'] },
            { 'datetime' => '2020:06:19 07:51:05', 'flash' => 'false', 'author' => 'Jane Doe', 'location' => 'Tokyo, Japan', 'tags' => ['MoMAK', 'Modern art', 'Cherry blossom'] },
            { 'datetime' => '2019:07:22 09:45:17', 'author' => 'John Doe', 'location' => 'Tokyo, Japan' },
            { 'datetime' => '2019:07:22 09:41:31', 'author' => 'John Doe', 'location' => 'Paris, France', 'camera' => 'Apple iPhone 11', 'tags' => ['Modern art', 'Louvre'] }
          ].to_json
        )

        # Valid selector & default sort order
        query = { 'author' => 'Jane Doe', 'flash' => 'true' }
        code, mime, content = @responder.generate_page('/api/pictures', query)
        expect(code).to eql(200)
        expect(mime).to eql('application/json')
        expect(content).to eql(
          [
            { 'datetime' => '2020:06:20 18:14:09', 'flash' => 'true', 'author' => 'Jane Doe', 'location' => 'New York, USA', 'tags' => 'Urban photography' },
            { 'datetime' => '2020:06:20 06:09:54', 'flash' => 'true', 'author' => 'Jane Doe', 'location' => 'Paris, France', 'camera' => 'Canon EOS 5D MARK IV', 'tags' => ['Louvre', 'Pyramid', 'Urban photography'] }
          ].to_json
        )

        # Valid selector, valid sort order
        query = { 'author' => 'Jane Doe', 'tags' => 'Urban photography', 'sort_by' => 'datetime',
                  'sort_order' => 'asc' }
        code, mime, content = @responder.generate_page('/api/pictures', query)
        expect(code).to eql(200)
        expect(mime).to eql('application/json')
        expect(content).to eql(
          [
            { 'datetime' => '2020:06:20 06:09:54', 'flash' => 'true', 'author' => 'Jane Doe', 'location' => 'Paris, France', 'camera' => 'Canon EOS 5D MARK IV', 'tags' => ['Louvre', 'Pyramid', 'Urban photography'] },
            { 'datetime' => '2020:06:20 18:14:09', 'flash' => 'true', 'author' => 'Jane Doe', 'location' => 'New York, USA', 'tags' => 'Urban photography' }
          ].to_json
        )
        query = { 'author' => 'Jane Doe', 'flash' => 'true', 'sort_by' => 'location',
                  'sort_order' => 'desc' }
        code, mime, content = @responder.generate_page('/api/pictures', query)
        expect(code).to eql(200)
        expect(mime).to eql('application/json')
        expect(content).to eql(
          [
            { 'datetime' => '2020:06:20 06:09:54', 'flash' => 'true', 'author' => 'Jane Doe', 'location' => 'Paris, France', 'camera' => 'Canon EOS 5D MARK IV', 'tags' => ['Louvre', 'Pyramid', 'Urban photography'] },
            { 'datetime' => '2020:06:20 18:14:09', 'flash' => 'true', 'author' => 'Jane Doe', 'location' => 'New York, USA', 'tags' => 'Urban photography' }
          ].to_json
        )

        # Invalid selector
        query = { 'a' => 'b' }
        code, mime, content = @responder.generate_page('/api/pictures', query)
        expect(code).to eql(200)
        expect(mime).to eql('application/json')
        expect(content).to eql([].to_json)

        # Invalid sort order
        query = { 'sort_order' => 'foo' }
        code, mime, content = @responder.generate_page('/api/pictures', query)
        expect(code).to eql(404)
        expect(mime).to be_empty
        expect(content).to be_empty
      end
    end

    context 'when asked for \'/api/picture\'' do
      it 'should return the selected picture' do
        # Existing picture
        query = { 'uri' => 'white.jpg' }
        code, mime, content = @responder.generate_page('/api/picture', query)
        expect(code).to eql(200)
        expect(mime).to eql('image/jpeg')
        expect(content).to eql(File.read(File.join(__dir__, 'white.jpg')))

        # Invalid selector
        query = { 'value' => true }
        code, mime, content = @responder.generate_page('/api/picture', query)
        expect(code).to eql(404)
        expect(mime).to be_empty
        expect(content).to be_empty
      end
    end

    context 'otherwise' do
      it 'should return an HTTP 404 error' do
        expect(@responder.generate_page('index.html', {})).to eql([404, '', ''])
        expect(@responder.generate_page('/api/groups', {})).to eql([404, '', ''])
        expect(@responder.generate_page('/api/group', {})).to eql([404, '', ''])
        expect(@responder.generate_page('/api/pictures/foo', {})).to eql([404, '', ''])
      end
    end
  end
end
